# Flashing Tasmota firmware to Tuya smart plug

Despite being affordable, smart plugs available from Aliexpress has one defect. In order 
to use it you to install proprietary application, create and login to Tuya account. 
Accessing and controlling a it using a browser and incorrporating smart plug to a local 
Home Assistant instance seemed imposible using existing Tuya firmware that device comes with,
without a hassle of using [Tuya Smart Life](https://developer.tuya.com/en/docs/iot/tuya-smart-app-smart-life-app-advantages?id=K989rqa49rluq#title-1-Download) and creating [Tuya IoT Platform](https://iot.tuya.com/) account. Home Assistant offers Tuya integration that makes this easy, but it seems that third party Cloud solutions 
are unnecessary and overkill in order to use the basic device functionality which is simply turning the relay on and off.

## Tasmota
TODO: short about Tasmota and links to Tasmota page

## OpenBeken firmware

TODO: what is open beken
https://openbekeniot.github.io/webapp/devicesList.html
https://github.com/openshwprojects/OpenBK7231T_App

## Smart plug disassembly

https://www.elektroda.com/rtvforum/topic3937910.html

### CB2S module